module FixturesHelper
  def read_fixture(path)
    Rails.root.join("spec/fixtures").join(path).read
  end
end

RSpec.configure do |c|
  c.include FixturesHelper
end
