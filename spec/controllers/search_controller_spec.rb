require 'rails_helper'

RSpec.describe SearchController, type: :controller do
  describe 'GET #search' do
    context 'with params' do
      subject { get :search, params: { phrase: 'test', engine: 'phonearena' } }

      it 'responds with 200' do
        expect(subject).to have_http_status(200)
      end

      it 'responds with uuid' do
        subject
        expect(JSON.parse(response.body)).to have_key('uuid')
      end
    end

    context 'without params' do
      it 'responds with 422' do
        expect( get :search ).to have_http_status(422)
      end
    end

    context 'response data' do
      let(:params) { { 'uuid' => 'uuid' } }
      let(:data) {
        {
          'name' => "Apple iPhone 7",
          'dimensions' => '5.44 x 2.64 x 0.28 inches  (138.3 x 67.1 x 7.1 mm)'
        }
      }
      subject { get :search, params: params }

      it 'responds with uuid while searching' do
        subject

        expect(JSON.parse(response.body)).to eq(params)
      end

      it 'responds with data' do
        Rails.cache.write(params['uuid'], data)

        subject

        expect(JSON.parse(response.body)).to eq(data)
      end
    end
  end
end
