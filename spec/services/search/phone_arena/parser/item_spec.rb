require 'rails_helper'

RSpec.describe Search::PhoneArena::Parser::Item do
  let(:raw_html) { read_fixture('parsers/phone_arena/item.html') }
  let(:service) { Search::PhoneArena::Parser::Item.new raw_html }
  let(:result) {
    {
        name: "Apple iPhone 7",
        dimensions: '5.44 x 2.64 x 0.28 inches  (138.3 x 67.1 x 7.1 mm)'
    }
  }

  it 'parses item html' do
    expect(service.parse).to eq(result)
  end
end
