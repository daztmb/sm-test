RSpec.describe Search::PhoneArena::Parser::Search do
  let(:raw_html) { read_fixture('parsers/phone_arena/search.html') }
  let(:service) { Search::PhoneArena::Parser::Search.new raw_html }
  let(:result) {
    [
        '/phones/Apple-iPhone-7-Plus_id9816',
        '/phones/Apple-iPhone-7_id9815'
    ]
  }

  it 'parses item html' do
    expect(service.parse).to eq(result)
  end
end
