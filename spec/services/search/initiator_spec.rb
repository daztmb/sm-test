require 'rails_helper'

RSpec.describe Search::Initiator do
  let(:phrase) { 'iphone 7' }
  let(:engine) { 'phonearena' }
  let(:service) { Search::Initiator.new phrase, engine }
  let(:result) {
    {
        name: 'Apple iPhone 7 Plus',
        dimensions: '6.23 x 3.07 x 0.29 inches  (158.2 x 77.9 x 7.3 mm)'
    }
  }

  it 'make search' do
    VCR.use_cassette 'iphone7_phonearena_search' do
      expect(service.process).to eq(result)
    end
  end
end
