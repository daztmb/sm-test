class Remote::PhoneArena::Client < Remote::Client

  base_url 'https://www.phonearena.com'

  def search phrase
    get('/search', { term: phrase })
  end

end
