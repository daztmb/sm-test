require 'http'

class Remote::Client

  def self.base_url url
    @@base_url = url
  end

  def load_path path
    get(path)
  end

  private

  def build_url(path)
    "#{@@base_url}#{path}"
  end

  def get(path, params={})
    HTTP.get(build_url(path), params: params).to_s
  end

end
