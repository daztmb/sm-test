require 'securerandom'

class SearchController < ApplicationController

  before_action :validate_params, only: :search

  def search
    set_data

    render json: @data
  end

private

  def init_search
    uuid = SecureRandom.uuid

    SearchWorker.perform_async(uuid, params[:phrase], params[:engine])

    uuid
  end

  def set_data
    @data = if params[:uuid]
              Rails.cache.read(params[:uuid]) || { uuid: params[:uuid] }
            else
              { uuid: init_search }
            end
  end

  def validate_params
    head(:unprocessable_entity) and return unless ((params[:phrase] && params[:engine]) || params[:uuid])
  end

end
