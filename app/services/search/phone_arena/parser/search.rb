class Search::PhoneArena::Parser::Search < Search::PhoneArena::Parser::Base

  def parse
    @html.css('#search_results > #phones > .s_listing > div > a').map{|el| el.attr('href')}
  end

end
