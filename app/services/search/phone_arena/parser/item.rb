class Search::PhoneArena::Parser::Item < Search::PhoneArena::Parser::Base

  def parse
    {
        name: name,
        dimensions: dimensions
    }
  end

private

  def name
    @html.at('#phone > h1 > span')&.text
  end

  def dimensions
    @html.at('#phone #phone_specificatons #design_cPoint .field-6 ul')&.text
  end

end
