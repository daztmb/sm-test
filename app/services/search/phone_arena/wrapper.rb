class Search::PhoneArena::Wrapper

  def initialize phrase
    @phrase = phrase
    @client = Remote::PhoneArena::Client.new
  end

  def process
    path = search_first_match

    return {} unless path

    get_item_info path
  end

private

  def search_first_match
    Search::PhoneArena::Parser::Search.new(@client.search @phrase).parse.first
  end

  def get_item_info path
    Search::PhoneArena::Parser::Item.new(@client.load_path path).parse
  end

end
