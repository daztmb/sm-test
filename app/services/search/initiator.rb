class Search::Initiator

  SITES = {
      phonearena: 'PhoneArena'
  }

  def initialize phrase, engine
    @phrase = phrase
    @engine = engine
  end

  def process
    SITES.each_pair do |key, value|
      next unless @engine == key.to_s

      return "Search::#{value}::Wrapper".constantize.new(@phrase).process
    end

    return {}
  end
end
