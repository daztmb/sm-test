$(document).ready(function () {
  function search(method, url, data) {
    return $.ajax({
      type: method,
      url: url,
      data: data,
      beforeSend: function() {
        showLoader();
      },
      success: function(data) {
        if (data.uuid) {
          setTimeout(function() { search(method, url, { uuid: data.uuid }) }, 1500);
        } else {
          if (data.name) {
            hideLoader(`${data.name}: ${data.dimensions}`);
          } else {
            hideLoader('Not Found');
          }
        }
      },
      error: function() {
        hideLoader('Error');
      }
    });
  };

  function showLoader() {
    $('#progress').removeClass('d-none');
    $('#result').text('');
  };

  function hideLoader(text) {
    $('#progress').addClass('d-none');
    $('#result').text(text);
  };

  $('#search-form').submit(function(e) {
    e.preventDefault();

    var form = $(e.currentTarget);

    search(form.attr('method'), form.attr('action'), form.serialize())
  });
});
