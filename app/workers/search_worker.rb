class SearchWorker
  include Sidekiq::Worker

  def perform(uuid, phrase, engine)
    result = Search::Initiator.new(phrase, engine).process

    Rails.cache.write(uuid, result)
  end
end
